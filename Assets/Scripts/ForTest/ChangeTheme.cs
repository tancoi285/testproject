﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTheme : MonoBehaviour {
    [System.Serializable]
    public class ThemeInfo {
        public Theme Theme;
        public Texture Texture;
    }

    public enum Theme {
        Day, Night
    }

    [SerializeField] private Theme mainTheme;
    [SerializeField] private List<ThemeInfo> themeInfos;

    private void Start() {
        SetTheme();
    }
    
    void SetTheme() {
        ThemeInfo info = themeInfos.Find(x => x.Theme == mainTheme);
        if (info == null) return;
        if (info.Texture == null) return;
        transform.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", info.Texture);
    }
}
