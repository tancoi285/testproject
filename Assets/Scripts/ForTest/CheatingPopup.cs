﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatingPopup : MonoBehaviour {
    public void AddMagnet() {
        PlayerData.instance.Add(Consumable.ConsumableType.COIN_MAG, 100);
    }

    public void AddFish() {
        PlayerData.instance.coins += 10000;
        PlayerData.instance.Save();
    }

    public void AddPremium() {
        PlayerData.instance.premium += 100;
        PlayerData.instance.Save();
    }

    public void AddX2() {
        PlayerData.instance.Add(Consumable.ConsumableType.SCORE_MULTIPLAYER, 100);
    }

    public void AddInvincible() {
        PlayerData.instance.Add(Consumable.ConsumableType.INVINCIBILITY, 100);
    }

    public void AddLife() {
        PlayerData.instance.Add(Consumable.ConsumableType.EXTRALIFE, 100);
    }
}
