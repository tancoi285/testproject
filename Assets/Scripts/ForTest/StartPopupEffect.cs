﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StartPopupEffect : MonoBehaviour {
    private void OnEnable() {
        DoScaleEffect();
    }

    void DoScaleEffect() {
        Vector2 baseScale = transform.localScale;
        transform.localScale = Vector3.zero;
        transform.DOScale(baseScale, 0.15f);
    }
}
