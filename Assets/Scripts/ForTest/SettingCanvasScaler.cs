﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingCanvasScaler : MonoBehaviour {
    [SerializeField] private CanvasScaler canvas;

    private void Start() {
        if (canvas == null) {
            canvas = transform.GetComponent<CanvasScaler>();
        }
        SettingMatch();
    }

    float Ratio(Vector2 size) {
        return size.x / size.y;
    }

    void SettingMatch() {
#if UNITY_EDITOR
        Vector2 size = UnityEditor.Handles.GetMainGameViewSize();
#else
        Vector2 size = new Vector2(Screen.width, Screen.height);
#endif
        Vector2 baseSize = new Vector2(1080, 1920);
        float match = Ratio(baseSize) >= Ratio(size) ? 0 : 1;
        canvas.matchWidthOrHeight = match;
    }
}
